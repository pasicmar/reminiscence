let socket
let color = '#000'
let strokeWidth = 0.7
var cv

document.addEventListener('contextmenu', event => event.preventDefault());

function setup() {
    //pixelDensity(1);
	cv = createCanvas(windowWidth, windowHeight);
	//centerCanvas();
	cv.background(255, 255, 255);
    //noSmooth();

    /*
    var canvasElt = document.querySelector('canvas');
    var stream = canvasElt.captureStream(25);
    socket = io.connect('http://localhost:3000')
    socket.on('mouse', data => {
		stroke(data.color)
		strokeWeight(data.strokeWidth)
		line(data.x, data.y, data.px, data.py)
	})
    */ 
}

function windowResized() {
	//centerCanvas();
	resizeCanvas(windowWidth, windowHeight, true);
}

function mouseDragged() {
	stroke(color);
	strokeWeight(strokeWidth);
	line(mouseX, mouseY, pmouseX, pmouseY);
	//sendmouse(mouseX, mouseY, pmouseX, pmouseY)
}

function touchMoved() {
    stroke(color);
	strokeWeight(strokeWidth);
	line(mouseX, mouseY, pmouseX, pmouseY);
    return false;
}


/*
function centerCanvas() {
	const x = (windowWidth) / 700;
	const y = (windowHeight) / 400;
	cv.position(x, y);
};
*/

/*
function touchStarted() {
	stroke(color);
	strokeWeight(strokeWidth);
	line(mouseX, mouseY, pmouseX, pmouseY);
	//sendmouse(mouseX, mouseY, pmouseX, pmouseY)
}
*/

/*
function sendmouse(x, y, pX, pY) {
	const data = {
		x: x,
		y: y,
		px: pX,
		py: pY,
		color: color,
		strokeWidth: strokeWidth,
	}

	socket.emit('mouse', data)
}
*/

/*

function draw() {
    rect(10, 10, 300, 40);
    text(frameRate(),20,30);
}
*/



