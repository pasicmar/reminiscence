function validateForm(event) {
    var errors = [];

    var emailEl = document.getElementById("email");
    var emailValue = emailEl.value;

    if (emailValue.length === 0) {
        errors.push({
            message: "sorry, if you don't submit any email, there is no reminiscence waiting for you...",
            element: emailEl
        });
    } else if (emailValue.length < 5) {
        errors.push({
            message: "your identity is kind of too short",
            element: emailEl
        });
    } else if (emailValue.length > 30) {
        errors.push({
            message: "sorry, your identity is too long",
            element: emailEl
        });
    } else if (!emailValue.includes("@")) {
        errors.push({
            message: "we are sorry for inconvenience, but we miss the monkey",
            element: emailEl
        });
    }

function AddUser() {
    var frm = $('#email');
    $.ajax({
         type: 'POST',
         url: 'index.php',
         data: frm.serialize(),
         success: function (data) {
            // console.log(data);
            if(data == 5){
                allCorrect();
                    }
                else alreadyExist();
         },
        error: function (data) {
            console.log('An error occurred.');
         }
     })

}


var formValid = errors.length === 0;
var errorMessageEl = document.getElementById("error");


if (formValid) {
    AddUser();
    } else { showError();
}

function showError(){
    var firstError = errors[0];
    errorMessageEl.hidden = false;
    errorMessageEl.innerText = firstError.message;
    firstError.element.focus();
}
function allCorrect(){
    errorMessageEl.hidden = true;
    errorMessageEl.innerText = "";
    window.location.href = "canvas.html";
    }

function alreadyExist(){
    errors.push({
        message: "~~~ this mail was already used ~~~",
        element: emailEl
    });
   showError();
}

return formValid;
}
